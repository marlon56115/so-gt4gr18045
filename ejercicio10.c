#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct
{
  char nombre[40];
  int dui;
  float sueldo;
} empleado;

void capturarDatos(empleado *[], int numEmpleados);
void ordenarPorNombre(empleado *[], int numEmpleados);
void imprimirArreglo(empleado *[], int numEmpleados);
void ordenarPorSueldo(empleado *[], int numEmpleados);
float promedioSueldos(empleado *[], int numEmpleados);

int main()
{
  empleado *empleados[1000]; //arreglo de punteros que apuntan a struc tipo empleado

  int numEmpleados;
  printf("ingrese cuantos empleados ingresara: ");
  scanf("%i", &numEmpleados);
  capturarDatos(empleados, numEmpleados);
  ordenarPorNombre(empleados, numEmpleados);
  printf("\nEmpleados rdenados por nombre:\n");
  imprimirArreglo(empleados, numEmpleados);
  ordenarPorSueldo(empleados, numEmpleados);
  printf("\nEmpleados rdenados por sueldo:\n");
  imprimirArreglo(empleados, numEmpleados);
  printf("\nPromedio de sueldos: %f\n", promedioSueldos(empleados, numEmpleados));
  return 0;
}

void capturarDatos(empleado *empleados[], int numEmpleados)
{
  for (int i = 0; i < numEmpleados; i++)
  {
    empleados[i] = (empleado *)malloc(sizeof(empleado));
    printf("ingrese el nombre del empleado %i\n:", i);
    //aqui funciona asi porque es un char[]
    scanf("%s", empleados[i]->nombre);
    printf("ingrese el DUI del empleado %i\n:", i);
    scanf("%i", &empleados[i]->dui);
    printf("ingrese el sueldo del empleado %i\n:", i);
    scanf("%f", &empleados[i]->sueldo);
  }
}

void ordenarPorNombre(empleado *empleados[], int numEmpleados)
{
  empleado *anterior = (empleado *)malloc(sizeof(empleado));
  for (int i = 0; i < numEmpleados - 1; i++)
  {
    for (int j = 0; j < numEmpleados - 1; j++)
    {
      if (strcmp(empleados[j]->nombre, empleados[j + 1]->nombre) > 0)
      {
        anterior = empleados[j];
        empleados[j] = empleados[j + 1];
        empleados[j + 1] = anterior;
      }
    }
  }
}

void imprimirArreglo(empleado *empleados[], int numEmpleados)
{
  for (int i = 0; i < numEmpleados; i++)
  {
    printf("empleado: %i\nnombre:%s\ndui:%i\nsueldo:%f\n", i, empleados[i]->nombre, empleados[i]->dui, empleados[i]->sueldo);
  }
  putchar('\n');
}

void ordenarPorSueldo(empleado *empleados[], int numEmpleados)
{
  empleado *anterior = (empleado *)malloc(sizeof(empleado));
  for (int i = 0; i < numEmpleados - 1; i++)
  {
    for (int j = 0; j < numEmpleados - 1; j++)
    {
      if (empleados[j]->sueldo < empleados[j + 1]->sueldo)
      {
        anterior = empleados[j];
        empleados[j] = empleados[j + 1];
        empleados[j + 1] = anterior;
      }
    }
  }
}

float promedioSueldos(empleado *empleados[], int numEmpleados)
{
  float total = 0;
  for (int i = 0; i < numEmpleados; i++)
  {
    total += empleados[i]->sueldo;
  }
  return total / numEmpleados;
}