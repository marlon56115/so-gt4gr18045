#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void ingresarPalabras(char *[], int numero);
void imprimirArreglo(char *[], int numeroPalabras);
void ordenarPalabras(char *[], int numeroPalabras);

int main()
{
  int numeroPalabras;
  printf("ingrese cuantas palabras: ");
  scanf("%i", &numeroPalabras);
  char *palabras[numeroPalabras]; //arreglo que almacenara las primeras direcciones de memoria de cada palabra

  ingresarPalabras(palabras, numeroPalabras);
  printf("\npalabras desordenadas:\n");
  imprimirArreglo(palabras, numeroPalabras);
  ordenarPalabras(palabras, numeroPalabras);
  printf("\npalabras ordenadas:\n");
  imprimirArreglo(palabras, numeroPalabras);

  return 0;
}

void ingresarPalabras(char *palabras[], int numeroPalabras)
{
  for (int i = 0; i < numeroPalabras; i++)
  {
    printf("ingrese la palabra %i: ", i + 1);
    palabras[i] = (char *)malloc(sizeof(20));
    //aqui funciona el scanf asi porque es un tipo char 
    scanf("%s", palabras[i]);
  }
}
void imprimirArreglo(char *palabras[], int numeroPalabras)
{
  for (int i = 0; i < numeroPalabras; i++)
  {
    puts(palabras[i]);
  }
  putchar('\n');
}

void ordenarPalabras(char *palabras[], int numeroPalabras)
{
  char *anterior = (char *)malloc(sizeof(20));
  for (int i = 0; i < numeroPalabras - 1; i++)
  {
    for (int j = 0; j < numeroPalabras - 1; j++)
    {
      if (strcmp(palabras[j], palabras[j + 1]) > 0)
      {
        anterior = palabras[j];
        palabras[j] = palabras[j + 1];
        palabras[j + 1] = anterior;
      }
    }
  }
}
