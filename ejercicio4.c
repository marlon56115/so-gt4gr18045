#include <stdio.h>

int main()
{
  int *pnumero;
  int num1, num2;
  char *pchar;
  char letra1;
  num1 = 2;
  num2 = 5;
  letra1 = 'a';
  pnumero = &num1;

  printf("num1=%i,num2=%i,pchar=%p,letra1=%c,pnumero=%p \n", num1, num2, pchar, letra1, pnumero);
  return 0;
}