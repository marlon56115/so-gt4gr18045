#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
  int grado;
  double evaluar, evaluado;
  char polinomio[100];
  printf("ingrese el grado del polinomio: ");
  scanf("%i", &grado);
  double *coeficientes[grado + 1];

  for (int i = 0; i <= grado; i++)
  {
    printf("ingrese el coeficiente %i: ", i + 1);
    coeficientes[i] = (double *)malloc(sizeof(double));
    scanf("%lf", coeficientes[i]);
  }

  printf("ingrese el valor a evaluar: ");
  scanf("%lf", &evaluar);

  for (int i = 0; i <= grado; i++)
  {
    evaluado += *coeficientes[i] * (pow(evaluar, i));
  }

  putchar('\n');
  printf("P(x)=");
  for (int i = 0; i <= grado; i++)
  {
    printf("%lf", *coeficientes[i]);
    printf("x^%i", i);
    if(i!=grado){
      putchar('+');
    }
  }
  
  putchar('\n');
  printf("P(%lf)=",evaluar);
  for (int i = 0; i <= grado; i++)
  {
    printf("%lf", *coeficientes[i]);
    printf("*%lf^%i", evaluar, i);
    if (i != grado)
    {
      putchar('+');
    }

  }
  putchar('\n');
  printf("P(%lf)=%lf\n", evaluar, evaluado);
}