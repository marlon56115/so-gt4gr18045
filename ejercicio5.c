#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int main()
{
  int *numeros[100];
  int upper = 100, lower = 0, actual = 0, mayor = 0;

  //asignamos los espacios de memoria dinamicamente y agregamos un valor aleatorio
  for (int i = 0; i <= 100; i++)
  {
    numeros[i] = (int *)malloc(sizeof(int));
    srand(i);
    *numeros[i] = (rand() % (upper - lower + 1)) + lower;
  }

  //ordenamos el arreglo de mayor a menor
  for (int i = 0; i <= 100 - 1; i++)
  {
    for (int j = 0; j <= 100 - 1; j++)
    {

      if (*numeros[j] < *numeros[j + 1])
      {
        actual = *numeros[j];
        *numeros[j] = *numeros[j + 1];
        *numeros[j + 1] = actual;
      }
    }
  }
  //imprimimos el arreglo ordenado
  for (int i = 0; i <= 100; i++)
  {
    printf("numeros[%i]=%i\n",i, *numeros[i]);
  }
  return 0;
}