#include <stdio.h>
#include <stdlib.h>

int main()
{
  int cantidad;
  float media, maximo, minimo,total=0;
  printf("ingrese el numero de datos a opear: ");
  scanf("%i", &cantidad);
  float *numeros[cantidad];

  for (int i = 0; i < cantidad; i++)
  {
    printf("ingrese el numero %i: ", i + 1);
    numeros[i] = (float *)malloc(sizeof(float));
    scanf("%f", numeros[i]);
  }

  //para encontrar el maximo
  maximo = *numeros[0];
  for (int i = 0; i < cantidad; i++)
  {
    if (maximo < *numeros[i])
    {
      maximo = *numeros[i];
    }
  }

  //para encontrar el minimo
  minimo = *numeros[0];
   for (int i = 0; i < cantidad; i++)
  {
    if (minimo > *numeros[i])
    {
      minimo = *numeros[i];
    }
  } 
  //imprimimos el arreglo
  putchar('\n');
  for (int i = 0; i < cantidad; i++)
  {
    printf("numeros[%i]=%f\n", i+1, *numeros[i]);
  }

  //calculando la media
  for (int i = 0; i < cantidad; i++)
  {
    total += *numeros[i];
  }

  //imprimimos el maximo y minimo y la media
  putchar('\n');
  printf("maximo:%f\nminimo:%f\nmedia:%f\n", maximo, minimo, (total/cantidad));

  return 0;
}