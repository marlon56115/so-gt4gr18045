#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *pedirTexto();
void contarVocales(char *, int[]);
void imprimir(int[]);

void main()
{
  char *texto;
  int num[5];
  texto = pedirTexto();
  contarVocales(texto, num);
  imprimir(num);
}

char *pedirTexto()
{
  printf("ingrese el texto:\n");
  char *text;
  text = (char *)malloc(20);
  scanf("%s", text);
  return text;
}

void contarVocales(char *texto, int num[])
{

  //hacemos esto para que no haya problemas con valores inesperados
  for (int i=0; i < 5;i++){
    num[i] = 0;
  }

    for (int i = 0; i <= strlen(texto); i++)
    {
      if (texto[i] == 'a' || texto[i] == 'A')
      {
        num[0]++;
      }
      if (texto[i] == 'e' || texto[i] == 'E')
      {
        num[1]++;
      }
      if (texto[i] == 'i' || texto[i] == 'I')
      {
        num[2]++;
      }
      if (texto[i] == 'o' || texto[i] == 'O')
      {
        num[3]++;
      }
      if (texto[i] == 'u' || texto[i] == 'U')
      {
        num[4]++;
      }
    }
}

void imprimir(int num[])
{
 
    printf("la a se repite %i veces\n", num[0]);
    printf("la e se repite %i veces\n", num[1]);
    printf("la i se repite %i veces\n", num[2]);
    printf("la o se repite %i veces\n", num[3]);
    printf("la u se repite %i veces\n", num[4]);
}